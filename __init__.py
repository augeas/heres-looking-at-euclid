

import badgelink
import bl00mbox
import leds
import machine
from st3m.application import Application, ApplicationContext
from st3m.input import InputController
import st3m.run

def euclid_rhythm(n, k):
    if k > n:
        n,k = k,n
    seq = [[True] for i in range(k)] + [[False] for j in range(n-k)]

    popped = True

    while seq[-1] == seq[-2] and popped: # Are there multiple copies of the same sequence at the end?
        popped = False
        last = seq[-1]

        for item in seq:
            if item == last:
                break # Run out of things to tack the sequence on to...
            else:
                if seq[-1] == last:
                    item += seq.pop()
                    popped = True
                else:
                    break # Run out of copies of the sequence.

    while True:
        for chunk in seq:
            yield from chunk

class Euclid(Application):
    def __init__(self, app_ctx):
        super().__init__(app_ctx)

        self.input = InputController()

        self.left = [3, 5, 7]
        self.right = [5, 7, 11]

        self.selected = 0
        self.debounce_up = [False for i in range(10)]
        self.debounce_down = [False for i in range(10)]

        self.elapsed = 0
        self.tempo = 150
        self.bpm = int(60000 / self.tempo)

        self.set_rhythms()

        self.samples = []

        self.loading = True
        self.which_led = 0
        self.go = True

        self.left_tip = None
        self.left_ring = None
        self.left_gate = False
        self.left_debounce = False
        self.right_debounce = False


        badgelink.right.enable()
        self.right_tip, self.right_ring = self.get_pin()

        self.pins = [
            self.left_ring,
            self.left_tip,
            self.right_ring,
            self.right_tip
        ]

        self.chan = bl00mbox.Channel()

    def set_rhythms(self):
        self.rhythms = [euclid_rhythm(*p) for p in
            (self.left[0:-1], self.left[1:], self.right[0:-1], self.right[1:])]

    def petal_pressed(self, petals, index, cw=True):
        if petals[index].pressed:
            if cw:
                return petals[index].pads.cw
            else:
                return petals[index].pads.ccw

    def petal_debounce(self, petals, index, action, args, cw=True, up=True):
        if up:
            debounce = self.debounce_up
        else:
            debounce = self.debounce_down

        if self.petal_pressed(petals, index, cw) and not debounce[index]:
            debounce[index] = True
            action(*args)

        if not self.petal_pressed(petals, index, cw):
            debounce[index] = False

    def select(self, delta):
        self.selected = (self.selected + delta) % 6

    def delta(self, d=1):
        if self.selected < 3:
            index = self.selected
            left = True
            trial = list(self.left)
        else:
            index = self.selected - 3
            left = False
            trial = list(self.right)

        trial[index] += d

        valid = (trial[0] != trial[1] and trial[1] != trial[2])
        if not valid:
            trial[index] += d

        valid = trial[index] > 0 and trial[index] < 100

        if valid:
            if left:
                self.left = trial
            else:
                self.right = trial

            self.set_rhythms()

    def set_tempo(self, up):
        if up and self.tempo >= 100:
            self.tempo -= 25

        if not up and self.tempo <= 975:
            self.tempo += 25

        self.bpm = int(60000 / self.tempo)

    def get_pin(self, right=True):
        if right:
            jack = badgelink.right
        else:
            jack = badgelink.left

        jack.enable()

        tip_pin = jack.tip.pin
        ring_pin = jack.ring.pin

        tip_pin.init(mode=machine.Pin.OUT)
        ring_pin.init(mode=machine.Pin.OUT)

        return tip_pin, ring_pin

    def think(self, ins: InputState, delta_ms: int):
        self.input.think(ins, delta_ms)

        if self.loading:
            sample_names = ['kick.wav',  'open.wav', 'snare.wav', 'close.wav']
            self.samples = [self.chan.new(bl00mbox.patches.sampler, sample)
                for sample in sample_names]

            for sample in self.samples:
                sample.signals.output = self.chan.mixer

            self.loading = False
        else:
            self.elapsed += delta_ms
            if self.elapsed >= self.tempo:
                self.elapsed = 0

                triggers = [r.__next__() for r in self.rhythms]

                if triggers[0]:
                    red = 1.0
                else:
                    red = 0.0
                if triggers[1]:
                    green = 0.5
                else:
                    green = 0.0
                if triggers[2]:
                    green += 0.5
                if triggers[3]:
                    blue  = 1.0
                else:
                    blue = 0.0

                leds.set_rgb(self.which_led, red, green, blue)
                leds.update()
                self.which_led += 1
                if self.which_led == 40:
                    self.which_led = 0

                if self.go:
                    for sample, trigger in zip(self.samples, triggers):
                        if trigger:
                            sample.signals.trigger.start()

                        for pin, trigger in zip(self.pins, triggers):
                            if not pin is None:
                                if trigger:
                                    pin.on()
                                else:
                                    pin.off()

        petals = ins.captouch.petals

        self.petal_debounce(petals, 8, self.select, [1], cw=True, up=True)

        self.petal_debounce(petals, 8, self.select, [-1], cw=False, up=False)

        self.petal_debounce(petals, 2, self.delta, [1], cw=False, up=True)

        self.petal_debounce(petals, 2, self.delta, [-1], cw=True, up=False)

        self.petal_debounce(petals, 0, self.set_tempo, [False], cw=False, up=True)

        self.petal_debounce(petals, 0, self.set_tempo, [True], cw=True, up=False)

        if self.input.buttons.app.right.pressed and not self.right_debounce:
            self.go = not self.go
            self.right_debounce = True

        if not self.input.buttons.app.right.pressed:
            self.right_debounce = False

        if self.input.buttons.app.left.pressed and not self.left_debounce:

            self.left_gate = not self.left_gate
            self.left_debounce = True

            if self.left_gate:
                self.left_tip, self.left_ring = self.get_pin(right=False)
            else:
                jack = badgelink.left
                jack.tip.disable()
                jack.ring.disable()
                jack.disable()
                self.left_tip = None
                self.left_ring = None

            self.pins[0] = self.left_ring
            self.pins[1] = self.left_tip

        if not self.input.buttons.app.left.pressed:
            self.left_debounce = False

    def draw(self, ctx):
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        left_text = [('00' + str(i))[-2:] for i in self.left]
        right_text = [('00' + str(i))[-2:] for i in self.right]

        for i in range(3):
            ctx.move_to(-55 + i * 40, -60)

            if self.selected == i:
                ctx.rgb(1.0, 0.0, 0.0)
            else:
                ctx.rgb(0.0, 1.0, 0.0)

            ctx.text(left_text[i])

            if self.selected == i + 3:
                ctx.rgb(1.0, 0.0, 0.0)
            else:
                ctx.rgb(0.0, 1.0, 0.0)

            ctx.move_to(-50 + i * 40, 60)
            ctx.text(right_text[i])

        ctx.move_to(-25, 0)
        ctx.rgb(0.0, 1.0, 1.0)
        ctx.text(str(self.bpm))

if __name__ == '__main__':
    st3m.run.run_view(Euclid(ApplicationContext()))
