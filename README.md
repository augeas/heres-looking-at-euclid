# Here's Looking at Euclid

### [Euclidean Rhythms](http://cgm.cs.mcgill.ca/~godfried/publications/banff.pdf).

```
Here's looking at Euclid
Celebrate years
Here's looking at Euclid
Wipe away tears
Long time, since we're together
Now I hope it's forever
```
--[Roxy Music](https://youtu.be/UP2KwDWSAV0?si=MUr9-aYduxF05SLw), *probably*.

Two sets of three integers produce four [Euclidean rhythms](http://cgm.cs.mcgill.ca/~godfried/publications/banff.pdf), one for each left-most or right-most pair.

The left petal selects the integer to modify.

The right petal changes the value.
(It will skip values to avoid zero division.)

The top petal controls tempo.

By default, the left jack socket outputs four rhythms by triggering the default samples. The right socket outputs gate signals from the two rhythms from the lower row of integers. Press the *left* shoulder button *left* to toggle sending gate signals from the rhythms from the upper row of integers from the left jack socket. This will also toggle the BPM display between cyan and yellow. 

Press the *left* shoulder button *right* to stop and start the sequence, as indicated byt whether the LEDs are moving.



